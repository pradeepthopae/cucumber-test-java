package skeleton;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty",
        "json:target/cucumber.json" })
/*@CucumberOptions(strict = false, features = "features", format = { "pretty",
        "json:target/cucumber.json" }, tags = { "~@ignore" })*/
public class RunCukesTest {
}
